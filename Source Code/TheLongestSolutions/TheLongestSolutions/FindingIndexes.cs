﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TheLongestSolutions
{  
    class FindingIndexes
    {
        List<int> indexesOfDuplicates = new List<int>(); // contains two integers, index of the leftmost and rightmost of the dublicates
        List<int> indexes = new List<int>(); // list of the found indexes
        List<int> highIndexes = new List<int>(); // list of the found indexes
        List<int> rangeResult = new List<int>(); // related to duplicates
        List<int> sortedList; // index list of sorted elements
        List<List<int>> solutionList = new List<List<int>>();
        List<int> solution = new List<int>();
        int i, j, z = 0;
        int IndexOfMaxMinor=0;
        int binarySearchResult;
        int originalIndex; // row index of the founded element in the excel file
        int solutionNo = 0;

        Decimal firstDifference = 0; 
        Decimal sum = 0;
        public static Decimal totalValue;
        public bool completed = false; 
        public FindingIndexes(List<int> sortedListValues, Decimal[] sortedArrayOfDecimals)
        {
            sortedList = new List<int>();
            foreach (var sortedListItem in sortedListValues)
            {
                sortedList.Add(sortedListItem);
            }
        }

        /**
        * Method that gets a new value from user and searches for it.
        * if found calls updateIndexes method, if not found calls
        * findClosestMax, finds the closest value and by calling 
        * IsDifferenceSmallerThanX method, searching is started.
        * @param sortedArrayOfDecimals sorted array of Comparable items.
        */
        public List<List<int>> searchNewTotal(Decimal[] sortedArrayOfDecimals, Decimal total)
        {
            totalValue = total;
            indexes.Clear();
            highIndexes.Clear();
            rangeResult.Clear();
            i = 0;
            z = 0;
            j = 0;
            solutionList.Clear();
            // looks for the data using binary search            
            int indexOfTotal = binarySearch(sortedArrayOfDecimals, 0, sortedArrayOfDecimals.Length-1, total);
            if (indexOfTotal >= 0) // Found
            {
                InCaseDuplicatesExistorNot(sortedArrayOfDecimals, total, indexOfTotal, IndexOfMaxMinor);
            }
            if (indexOfTotal != 0)
            {
                List<Decimal> arr = sortedArrayOfDecimals.ToList();
                IndexOfMaxMinor = findClosestMax(arr, total);
                if (IndexOfMaxMinor == 0)
                    Console.WriteLine("Not Found..");
                else
                {
                    IsDifferenceSmallerThanX(sortedArrayOfDecimals, IndexOfMaxMinor, total);
                    if (IndexOfMaxMinor == 0)
                    { 
                        return solutionList;
                    }
                }
            }
            return solutionList;
        }

        

        /**
        * Method that displays the recent founded solution. Stops the program
        * or returns in order to search another solution if there is
        * @param arr sorted array of Comparable items.
        * @param newIndexes new founded indexes
        */
        private void updateIndexes(Decimal[] arr, List<int> newIndexes)
        {
            solutionNo++;
            //Console.WriteLine("\n"); 
            //Console.WriteLine("Array Index");
            if (highIndexes.Count > 0)
            {
                for (int j = 0; j < highIndexes.Count; j++)
                {
                    indexes.Add(highIndexes[j]);
                }
            }
            foreach (int index in indexes) 
            {
                sum = sum + arr[index];
                //Console.Write(arr[index] + "(" + index + ". Index)" + " , ");
            }
            //Console.Write(" \n=> Sum is " + sum);
            if (sum == totalValue)
            {
                //Console.WriteLine("\n");
                //Console.WriteLine("Original Index(in the excel)");

                foreach (int index in indexes)
                {
                    originalIndex = sortedList.ElementAt(index) + 1;
                    //Console.Write(arr[index] + "(" + originalIndex + ". Index)" + " , ");
                    solution.Add(originalIndex);
                }
                List<int> sl = new List<int>();
                foreach (var it in solution)
                {
                    sl.Add(it);
                }
                solutionList.Add(sl);
            }
            sum = 0; 
            solution.Clear();
            highIndexes.Clear();
            indexes.Clear();
            return;
        }


        /**
        * Method that finds the difference between total and arr[IndexOfMaxMinor] and controls if 
        * Difference is smaller or bigger than the value in (IndexOfMaxMinor-1). index
        * @param arr sorted array of Comparable items.
        * @param IndexOfMaxMinor the index of the nearest and smaller value to the total value
        * @param total the value to be searched in an array of doubles 
        */
        public void IsDifferenceSmallerThanX(Decimal[] arr, int IndexOfMaxMinor, Decimal total)
        {
            if (IndexOfMaxMinor == 1)
            { 
            }

            Decimal difference = total - arr[IndexOfMaxMinor];
            if ((IndexOfMaxMinor == 0)&&(difference == 0))
            {
                highIndexes.Add(IndexOfMaxMinor);
                FinalIndexOfMaxMinor(IndexOfMaxMinor);
                return;
                //Console.WriteLine("Cant go on anymore!!");
                //getNewTotal(arr);
            }
            else if ((IndexOfMaxMinor == 0) && (difference > 0))
            {
                highIndexes.Clear();
                return;
            }
            else if( IndexOfMaxMinor >= 1)
            {
                if (difference < arr[IndexOfMaxMinor - 1])
                {
                    decreaseIndexOfMaxMinor(arr, IndexOfMaxMinor, total);
               
                }
                else if (difference > arr[IndexOfMaxMinor - 1])
                {
                    highIndexes.Add(IndexOfMaxMinor);
                    //indexes.Add(IndexOfMaxMinor - 1);
                    IndexOfMaxMinor--;
                    FinalIndexOfMaxMinor(IndexOfMaxMinor);
                    IsDifferenceSmallerThanX(arr, IndexOfMaxMinor, difference);
                    if ((IndexOfMaxMinor - 1) >= 0)
                        IsDifferenceSmallerThanX(arr, IndexOfMaxMinor - 1, difference);
                } 
                else if (difference == arr[IndexOfMaxMinor - 1])
                {
                    indexes.Add(IndexOfMaxMinor);
                    indexes.Add(IndexOfMaxMinor - 1);
                    updateIndexes(arr, indexes);
                }
            }
            
        }


        /**
        * Method that finds the difference between total and arr[IndexOfMaxMinor],calls findIndexes
        * method, if not found there, decreases the index of IndexOfMaxMinor, calculates the new 
        * difference between total and arr[IndexOfMaxMinor] and calls findIndexes method again.
        * @param arr sorted array of Comparable items.
        * @param IndexOfMaxMinor the index of the nearest and smaller value to the total value
        * @param total the value to be searched in an array of doubles
        */
        public void decreaseIndexOfMaxMinor(Decimal[] arr, int IndexOfMaxMinor, Decimal total)
        {
            
            Decimal difference = total - arr[IndexOfMaxMinor];
            binarySearchResult = binarySearch(arr, 0, IndexOfMaxMinor-1, difference);

            if (binarySearchResult >= 0)
            {
                indexes.Add(IndexOfMaxMinor);
                InCaseDuplicatesExistorNot(arr, difference, binarySearchResult, IndexOfMaxMinor);
            }
            firstDifference = difference;
            while ((arr[j] < difference)&&(j<arr.Length))
            {
                i = j;
                if (arr[i] != 0m)
                {
                    indexes.Add(IndexOfMaxMinor);
                    findIndexes(arr, difference, total, IndexOfMaxMinor);
                }
                else
                    j++;
            }
            j = 0;
            indexes.Clear();
            //IsDifferenceSmallerThanX(arr, IndexOfMaxMinor-1, total);
            IndexOfMaxMinor--;
            FinalIndexOfMaxMinor(IndexOfMaxMinor);
            z++;
            return;
            //if (IndexOfMaxMinor == 0)
            //    return;
            //IsDifferenceSmallerThanX(arr, IndexOfMaxMinor, total);
        }

        /**
        * Method that controls if duplicates of the value exist or not
        * and updates the list of the indexes according to the situation.
        * @param arr sorted array of Comparable items.
        * @param total the value to be searched in an array of doubles
        */
        public void InCaseDuplicatesExistorNot(Decimal[] arr, Decimal total, int binarySearchResult,int indexOfMx)
        {
            rangeResult = RangeSearch(arr, total);
            if (rangeResult.Count > 0)
            {
                for (int d = rangeResult.ElementAt(0); d < rangeResult.ElementAt(1); d++)
                {
                    indexes.Add(d);
                    //if (highIndexes.Capacity > 0)
                    //{
                    //    foreach (int highIndex in highIndexes)
                    //    {
                    //        indexes.Add(highIndex);
                    //    }
                    //}

                    updateIndexes(arr, indexes);
                    indexes.Add(indexOfMx);
                }
                rangeResult.Clear();
                indexes.Clear();
            }
            else
            {
                indexes.Add(binarySearchResult);
                //if (highIndexes.Capacity > 0)
                //{
                //    foreach (int highIndex in highIndexes)
                //    {
                //        indexes.Add(highIndex);
                //    }
                //}
                updateIndexes(arr, indexes);
                indexes.Clear();
            }

        }


        /**
        * Method that finds the indexes of the numbers in array of doubles, adds the indexes into a list
        * and makes recursive calls.
        * @param arr sorted array of Comparable items.
        * @param difference the difference between total and arr[IndexOfMaxMinor]
        * @param total the total value to be searched in an array of doubles
        * @param IndexOfMaxMinor the index of the nearest and smaller value to the total value
        */
        public void findIndexes(Decimal[] arr, Decimal difference, Decimal total, int IndexOfMaxMinor)
        {
            indexes.Add(i);
            difference = difference - arr[i];
            if (difference >= arr[i])
            {
                binarySearchResult = binarySearch(arr, i + 1, IndexOfMaxMinor-1, difference);
                if (binarySearchResult > 0)
                {
                    indexes.Add(binarySearchResult);
                    //if (highIndexes.Capacity > 0)
                    //{
                    //    foreach (int highIndex in highIndexes)
                    //    {
                    //        indexes.Add(highIndex);
                    //    }
                    //}
                    updateIndexes(arr, indexes);
                    indexes.Clear();

                    difference = firstDifference;
                    j++;
                    return;
                }
                else
                {
                    i++;
                    findIndexes(arr, difference, total, IndexOfMaxMinor);
                }
            }
            else
            {
                indexes.Clear();
                difference = firstDifference;
                j++;
                return;
            }

        }


        /**
        * Binary Search algorithm, searchs a value and returns the index of it.
        * @param arr sorted array of Comparable items.
        * @param bottom the index of the lower element
        * @param top the index of the upper element
        * @param lookingFor the value to be searched in an array of doubles
        */
        public int binarySearch(Decimal[] arr, int bottom, int top, Decimal lookingFor)
        {
            if (bottom <= top)
            {
                int ort;
                ort = (bottom + top) / 2; // the index of the mid element
                if (ort < arr.Length)
                {
                    if (lookingFor == arr[ort])
                    {
                        return ort; // found
                    }
                    else if (lookingFor < arr[ort])
                    {
                        return binarySearch(arr, bottom, ort - 1, lookingFor);
                    }
                    else
                        return binarySearch(arr, ort + 1, top, lookingFor);
                }
            }
            return -(bottom + 1); // not found
        }


        /**
        * Method that finds the indexes of the duplicates of a given value
        * @param arr sorted list of Comparable items.
        * @param lookingFor the value to be searched in a list of doubles
        */
        public List<int> RangeSearch(Decimal[] arr, Decimal lookingFor)
        {
            try
            {
                var duplicates = arr.GroupBy(g => g)
                        .Where(w => w.Count() > 1)
                        .Select(w => new { Value = w.Key, Count = w.Count() });

                int firstIndexOfDuplicate = arr.Select((item, index) => new
                {
                    ItemName = item,
                    Position = index
                }).Where(i => i.ItemName.ToString() == (lookingFor.ToString()))
                      .First()
                      .Position;
                foreach (var duplicate in duplicates)
                {
                    if (duplicate.Value.ToString().CompareTo(lookingFor.ToString()) == 0)
                    {
                        indexesOfDuplicates.Add(firstIndexOfDuplicate);
                        indexesOfDuplicates.Add(firstIndexOfDuplicate + duplicate.Count);
                        return indexesOfDuplicates;
                    }
                }
            }
            catch (Exception ex)
            {
                
                Console.WriteLine(ex.Message);
            }
            return indexesOfDuplicates;
        }


        /**
        * Method that finds the index of the closest smaller value to the total number
        * @param arr sorted list of Comparable items.
        * @param total the value to be searched in a list of doubles
        */
        public int findClosestMax(List<Decimal> arr, Decimal total)
        {

            var max = arr.Where(n => n < total).Max();
            int indexOfMax = arr.FindIndex(n => n == max);
           
            return indexOfMax;
        }
        public void FinalIndexOfMaxMinor(int finalIndexOfMM)
        {
            IndexOfMaxMinor = finalIndexOfMM;
        }
        public int FinalIndexOfMaxMinor()
        {
            return IndexOfMaxMinor;
        }
        public List<List<int>> SolutionList()
        {
            return solutionList;
        }
        public void CLearSolutionList()
        {
            solutionList.Clear();
        }

    }
}

