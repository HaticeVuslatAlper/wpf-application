﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using TheLongestSolutions.Alerts;
using WpfAnimatedGif;
using Excel = Microsoft.Office.Interop.Excel;
using System.ComponentModel;

namespace TheLongestSolutions
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
            int row = 17;
            int col = 20;
            string colName = "T";
            Decimal[] arrayDecimals;
            List<int> listIndexes = new List<int>();
            List<List<int>> sList;
            List<List<int>> previousSolutionList = new List<List<int>>();
            List<int> pSolution = new List<int>();
            OpenFileDialog fileDialog = new OpenFileDialog();
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            Effect effect = new Effect();
            string fileName = "";            
            string filePath = "";
            string extension = "";
            int IndexOfMaxMinor = 0;
            Decimal newValue;
            FindingIndexes fi;
            UploadingFile mc;
            String sltn;
            public event TextChangedEventHandler TextChanged;
            BackgroundWorker worker = new BackgroundWorker();  
            public MainWindow() 
            {
                
                InitializeComponent();
                FileProcess p = new FileProcess();
                //PersonViewModel pvm = new PersonViewModel() { Person = p };
                MainGrid.DataContext = p;
                
            }
            
            public void findIndexesObject(){
                mc = new UploadingFile();
                newValue = Convert.ToDecimal(TextBoxValue.Text);
                listIndexes = mc.SortedListOfIndexes();
                int length = mc.SortedArrayOfDecimals().Length;
                arrayDecimals = new Decimal[length];
                arrayDecimals = mc.SortedArrayOfDecimals();
                fi = new FindingIndexes(listIndexes, arrayDecimals);
            }

            private void clearAllTextBoxes()
            {
                TextBoxValue.Clear();
            }

            private void Browse_File()
            {
                ListView1.Items.Clear();
                //FilePath.Content = "";
                //ImportedData.Content = "";
                clearAllTextBoxes();
                fileDialog.Filter = "Excel documents (*.xls, *.xlsx)|*.xls;*.xlsx;*.xlsm";
                fileDialog.ShowDialog();

                if (!DialogResult.Equals("Cancel"))
                {
                    extension = System.IO.Path.GetExtension(fileDialog.FileName);
                    fileName = fileDialog.FileName;
                    filePath = fileName.Replace(@"\", "/");
                    //FilePath.Content = "File Path: " + filePath;
                    //FilePath.Visibility = Visibility.Visible;                  
                }
                
            }

            private void Import_Data(object sender, RoutedEventArgs e)
            {
                TextBoxValue.IsEnabled = false;
                //effect.ApplyEffect(Window.GetWindow(this));
                Browse_File();
                //effect.ClearEffect(Window.GetWindow(this));
                TextBoxValue.Clear();
                ListView1.Items.Clear();   
                UploadingFile mc = new UploadingFile();
               
                progress.Visibility = Visibility.Visible;
                worker.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    mc.UploadFile(filePath, row, col, colName, "all");

                };
                worker.RunWorkerAsync();

                worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    progress.Visibility = Visibility.Hidden;
                    TextBoxValue.IsEnabled = true;
                };
                //ImportedData.Visibility = Visibility.Visible;
               
                //ImportedData.Content = "Data is imported starting from " + row + "," + col + "(row,column)";
               
            }
           
           
            private void Search_New_Value(object sender, RoutedEventArgs e)
            {
               
                findIndexesObject();
                int final = listIndexes.Count - 1;
                Decimal finalElement = arrayDecimals[final];
                Decimal predict = finalElement * arrayDecimals.Length;

                if (newValue <= predict)
                {

                    previousSolutionList.Clear();
                    SearchAlert.Content = "";
                    ListView1.Items.Clear();

                    sList = fi.searchNewTotal(arrayDecimals, newValue);
                    Next.IsEnabled = true;
                    
                    if (sList.Count != 0)
                    {
                        AddSolutionsToListView(sList);
                    }
                    else
                    {
                        IndexOfMaxMinor = fi.FinalIndexOfMaxMinor();
                        do
                        {
                            fi.IsDifferenceSmallerThanX(arrayDecimals, IndexOfMaxMinor, newValue);
                            sList = fi.SolutionList();
                            IndexOfMaxMinor--;
                        } while ((sList.Count == 0) && (IndexOfMaxMinor >= 0));
                        if (sList.Count != 0)
                        {
                            AddSolutionsToListView(sList);
                        }
                    }
                }
                else
                {
                    SearchAlert.Visibility = Visibility.Visible;
                    SearchAlert.Content = "Couldn't find any solution!";
                }
               
            }

            private void AddSolutionsToListView(List<List<int>> sList)
            {
                
                foreach (var s in sList)
                {
                    String sltn = s.Count + " =>> ";
                    foreach (var i in s)
                    {
                        sltn += i.ToString() + "(" + arrayDecimals[listIndexes.IndexOf(i - 1)] + ")" + " , ";
                        pSolution.Add(i);
                    }
                    ListView1.Items.Add(sltn);
                    sltn = "";
                    List<int> psl2 = new List<int>();
                    foreach (var sl in pSolution)
                    {
                        psl2.Add(sl);
                    }
                    previousSolutionList.Add(psl2);
                    pSolution.Clear();
                }
                SelectSolution.IsEnabled = true;
            }
            
            private void Next_Solution(object sender, RoutedEventArgs e)
            {
                SearchAlert.Content = "";
                ListView1.Items.Clear();
                IndexOfMaxMinor = fi.FinalIndexOfMaxMinor();
                fi.CLearSolutionList();
                
                do
                {
                    fi.IsDifferenceSmallerThanX(arrayDecimals, IndexOfMaxMinor, newValue);
                    sList = fi.SolutionList();
                    IndexOfMaxMinor--;       
                } while ((sList.Count == 0) && (IndexOfMaxMinor>=1));
               
                if (sList.Count != 0)
                {
                    AddSolutionsToListView(sList);
                }
                else
                {
                    
                    foreach (var ps in previousSolutionList)
                    {
                        String sltn = ps.Count + " =>> ";
                        foreach (var s in ps)
                        {
                            sltn += s.ToString() + "(" + arrayDecimals[listIndexes.IndexOf(s - 1)] + ")" + " , ";
                        }
                        ListView1.Items.Add(sltn);
                        sltn = "";         
                    }
                    SelectSolution.IsEnabled = true;
                    Next.IsEnabled = false;
                    SearchAlert.Visibility = Visibility.Visible;
                    SearchAlert.Content = "Searching finished !!";
                }

            }


            private void Select_Solution(object sender, RoutedEventArgs e)
            {
                sltn = "";
                progress.Visibility = Visibility.Visible;
                List<int> selectedSolution= null;
                String selected = (String)ListView1.SelectedItems[0];
                int indexOfSolution = ListView1.Items.IndexOf(ListView1.SelectedItem);
                worker.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    if (sList.Count != 0)
                    {
                        selectedSolution = sList.ElementAt(indexOfSolution);
                    }
                    else if (previousSolutionList.Count != 0)
                    {
                        selectedSolution = previousSolutionList.ElementAt(indexOfSolution);
                    }
                
                    UploadingFile mc = new UploadingFile();
                
                    mc.MakeChangesInExcel(filePath, selectedSolution, extension);

                };
                worker.RunWorkerAsync();

                worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    progress.Visibility = Visibility.Hidden;
                    //SimpleAlert imageAlert = new SimpleAlert();

                    //imageAlert.Url = "http://www.google.com/intl/en_ALL/images/logo.gif";
                    //imageAlert.Url = "/Images/success.png";
                    //imageAlert.Title = "Notification";
                    //sltn ="";
                    //foreach (var s2 in selectedSolution)
                    //{
                    //    sltn += s2.ToString() + "(" + arrayDecimals[listIndexes.IndexOf(s2 - 1)] + ")" + " , ";
                    //}
                    //imageAlert.Message = "Selected Solution: " + sltn + "\n" + "The path of the updated copy: c:/users/public/workbook";
                    //foreach (var s2 in selectedSolution)
                    //{
                    //    sltn += s2.ToString() + "(" + arrayDecimals[listIndexes.IndexOf(s2 - 1)] + ")" + " , ";
                    //}
                    
                    //sltn = sltn.Substring(0,sltn.Length-2);
                    //sltn += "=> " + selectedSolution.Count;
                    MessageBox.Show("The path of the updated copy: \nc:/users/public/workbook");
                    
                };
                
            }

            private void TextBox_TextChanged(object sender, TextChangedEventArgs args)
            {
                ListView1.Items.Clear();
                Next.IsEnabled = false;
                SearchAlert.Visibility = Visibility.Hidden;
            }


            private void Import_SpecificData(object sender, RoutedEventArgs e)
            {
                TextBoxValue.IsEnabled = false;
                UploadingFile mc = new UploadingFile();
                worker.WorkerReportsProgress = true;

                //effect.ApplyEffect(Window.GetWindow(this));
                Browse_File();
                //effect.ClearEffect(Window.GetWindow(this));
                TextBoxValue.Clear();
                ListView1.Items.Clear();

                progress.Visibility = Visibility.Visible;
                worker.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    mc.UploadFile(filePath, row, col, colName, "specific");
  
                };
                worker.RunWorkerAsync();

                worker.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args) {
                    progress.Visibility = Visibility.Hidden;
                    TextBoxValue.IsEnabled = true;                  
                };
                
            }

    }
}
