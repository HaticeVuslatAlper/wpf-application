﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Windows.Controls;
 
namespace TheLongestSolutions
{
    public class TextBoxDecimalValidationRule : ValidationRule
    {
        private readonly List<CultureInfo> cultures = new List<CultureInfo> {
            CultureInfo.CurrentCulture,
            CultureInfo.CurrentUICulture,
            CultureInfo.InvariantCulture
        };
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            string input = (string)value;
           
            bool success = false;
            foreach (CultureInfo culture in cultures)
            {
                if (input.StartsWith("-"))
                    return new ValidationResult(false, input + " is not valid. You can't enter a negative value!");
                int length = input.Length;
                
                double result;
                if (double.TryParse(input, NumberStyles.AllowDecimalPoint | NumberStyles.Float, culture, out result))
                {
                    int decimalPos = input.IndexOf(culture.NumberFormat.NumberDecimalSeparator);
                    if (decimalPos == -1)
                    {
                        success = true;
                        break;
                    }
                   
                    if (input.Substring(decimalPos + 1).Length > 3)
                        return new ValidationResult(false, "Too many digits in " + input);

                    success = true;
                    break;
                }
               

            }
            return success ? ValidationResult.ValidResult : new ValidationResult(false, input + " is not a valid value!");

        }
    }
}
