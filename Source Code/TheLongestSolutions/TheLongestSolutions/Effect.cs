﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TheLongestSolutions
{ 
    class Effect
    {
        /// <summary>
        /// Apply Blur Effect on the window
        /// </summary>
        /// <param name=”win”></param>
        public void ApplyEffect(Window win)
        {
            System.Windows.Media.Effects.BlurEffect objBlur = new System.Windows.Media.Effects.BlurEffect();
            objBlur.Radius = 4;
            win.Effect = objBlur;
        }
        /// <summary>
        /// Remove Blur Effects
        /// </summary>
        /// <param name=”win”></param>
        public void ClearEffect(Window win)
        {
            win.Effect = null;
        }
    }
}
