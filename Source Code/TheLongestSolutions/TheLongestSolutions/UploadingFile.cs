﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

/////////////////////////////////////// Need and Solution ////////////////////////////////////////////////////////////////////////////////
//
// The company was using excel files to save information regarding all kinds of company expenses. The accounting team was supposed 
// to choose a specified amount of money from the expense list at the end of each period and they were doing this manually. The accounting 
// team wanted to see the solution lists of all possible combinations to collect that particular amount from the list of expenses so that 
// they could choose among them. Listed solutions contain indexes of the possible combinations. The first element shows the size of the solution.
// Solutions vary in terms of length, the number of expenses being taken into account since each has different amount from small to large. 
// So after choosing "select this solution" the selected combination/indexes are coloured to green in a copy of this excel file and saved in a 
// particular path. 
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/** 
 * This program reads data from the excel, adds them to a dictionary, sorts the array using dictionary sort,
 * adds the sorted elements' values into an array of decimals and the keys into a list at the same time,
 * searches the value in this sorted array of decimals and displays the results..
 **/

namespace TheLongestSolutions
{ 
    class UploadingFile
    {
        public static int rowCount; // number of rows
        public static int col; // index of the row, starting cell
        public static int row; // index of the col, starting cell
        public static int n = 0;
        public static int realRowCount; // number of rows filled with data
        public static Dictionary<int, Decimal> dictionary = new Dictionary<int, Decimal>();
        public static Decimal[] sortedArrayOfDecimals; // index array of sorted decimals
        public static List<int> sortedListOfIndexes = new List<int>();
        public static List<List<int>> filteredSList = new List<List<int>>();
        public static Object[,] valueArray;
        bool allEquals = false;
        int y = 0;

        public void UploadFile(String filePath, int rowi, int coli, string colName, string extractFrom)
        {
            dictionary.Clear(); 
            sortedListOfIndexes.Clear();
            n = 0;
            row = rowi; 
            col = coli;
            if (extractFrom == "all")
            {
                ExtractAllDataFromExcel(filePath, colName);
            }
            else if(extractFrom == "specific")
            {
                ExtractSpecificDataFromExcel(filePath, colName);
            }
            sortedArrayOfDecimals = new Decimal[realRowCount];
            Console.WriteLine("Sorting the array using dictionary sort ..");

            Stopwatch watch = new Stopwatch();
            watch.Start();
            var items = from pair in dictionary
                        orderby pair.Value ascending
                        select pair;
            watch.Stop();

            double elapsedTime = watch.ElapsedMilliseconds / 1000.0;
            double elapsedTicks = watch.ElapsedTicks;

            Console.WriteLine("Elapsed Time = " + elapsedTime);
            Console.WriteLine("Elapsed Ticks = " + elapsedTicks);
        

            //adds the original indexes of all sorted elements to a list of integers
            //and at the same time adds the values of these elements to an array of decimals           
            //so that when the array index of an element found, the original index of it can 
            //be reached with the same index from the list of the indexes   

            foreach (KeyValuePair<int, Decimal> pair in items)
            {
                //Console.WriteLine("{0}: {1}", pair.Key, pair.Value);
                sortedListOfIndexes.Add(pair.Key);
                sortedArrayOfDecimals[n] = pair.Value;
                //Console.WriteLine(pair.Value);
                n++;
            }
        }

        public List<int> SortedListOfIndexes()
        {
            return sortedListOfIndexes;
        }

        public Decimal[] SortedArrayOfDecimals()
        {
            return sortedArrayOfDecimals;
        }


        /**
        * Method that connects to an excel file and adds the data
        * from the excel to a dictionary 
        */
        private static void ExtractAllDataFromExcel(String filePath, string cName)
        {
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@filePath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            string columnName = cName + ":" + cName;
            Excel.Range xlRange = xlApp.Intersect(xlWorksheet.UsedRange, xlWorksheet.Columns[columnName, Type.Missing]);

            int rCount = xlRange.Rows.Count; 
            int colCount = xlRange.Columns.Count; 
            rowCount = rCount;
            Console.WriteLine("Data in the excel file is being added to a dictionary.. ");
            Stopwatch watch1 = new Stopwatch();
            watch1.Start();
            Object[,] valueArray = (Object[,])xlRange.get_Value(XlRangeValueDataType.xlRangeValueDefault);

            try
            {
                for (int rowId = row; rowId <= rowCount; rowId++)
                {
                    
                    Decimal value = Convert.ToDecimal(valueArray[rowId, 1]);
                    
                    if (value == 0)
                    {
                        realRowCount = rowId;
                        rowId = rowCount + 1;
                    }
                    else
                        dictionary.Add(rowId - 1, value);
                    realRowCount = dictionary.Count;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            watch1.Stop();
            double elapsedTime1 = watch1.ElapsedMilliseconds / 1000.0;
            double elapsedTicks1 = watch1.ElapsedTicks;

            Console.WriteLine("Elapsed Time = " + elapsedTime1);
            Console.WriteLine("Elapsed Ticks = " + elapsedTicks1);
            Console.ReadLine();

            object misValue = System.Reflection.Missing.Value;
            xlWorkbook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlRange);
            releaseObject(xlWorksheet);
            releaseObject(xlWorkbook);
            releaseObject(xlApp);
        }


        /**
        * Method that connects to an excel file and adds the data
        * from the excel to a dictionary
        */
        private static void ExtractSpecificDataFromExcel(String filePath, string cName)
        { 
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@filePath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            string columnName = cName + ":" + cName;
            Excel.Range xlRange = xlApp.Intersect(xlWorksheet.UsedRange, xlWorksheet.Columns[columnName, Type.Missing]);
            Excel.Range xlRangeSpecific = xlApp.Intersect(xlWorksheet.UsedRange, xlWorksheet.Columns["G:G", Type.Missing]);
            
            int rCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;
            rowCount = rCount;
            Console.WriteLine("Data in the excel file is being added to a dictionary.. ");
            Stopwatch watch1 = new Stopwatch();
            watch1.Start();
            Object[,] valueArray = (Object[,])xlRange.get_Value(XlRangeValueDataType.xlRangeValueDefault);
            Object[,] valueArraySpecific = (Object[,])xlRangeSpecific.get_Value(XlRangeValueDataType.xlRangeValueDefault);
            try
            {
                for (int rowId = row; rowId <= rowCount; rowId++)
                {

                    Decimal value = Convert.ToDecimal(valueArray[rowId, 1]);
                    String value2 = Convert.ToString(valueArraySpecific[rowId, 1]);
                    if (value == 0)
                    {
                        realRowCount = rowId;
                        rowId = rowCount + 1;
                    }
                    else{

                        if ((value2.Equals("İade alınamayan KDV") || value2.Equals("iade alınamayan kdv") || value2.Equals("İADE ALINAMAYAN KDV")))
                        {
                            dictionary.Add(rowId - 1, value);
                        }
                        
                    }
                    realRowCount = dictionary.Count;
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            watch1.Stop();
            double elapsedTime1 = watch1.ElapsedMilliseconds / 1000.0;
            double elapsedTicks1 = watch1.ElapsedTicks;

            Console.WriteLine("Elapsed Time = " + elapsedTime1);
            Console.WriteLine("Elapsed Ticks = " + elapsedTicks1);
            Console.ReadLine();

            object misValue = System.Reflection.Missing.Value;
            xlWorkbook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlRange);
            releaseObject(xlWorksheet);
            releaseObject(xlWorkbook);
            releaseObject(xlApp);
        }

        internal void MakeChangesInExcel(string filePath, List<int> selectedSolution, string extension)
        {

            //Process[] localByName = Process.GetProcessesByName("excel");
            //foreach (Process p in localByName)
            //{
            //    p.Kill();
            //}
            Excel.Application xlApp = new Excel.Application();        
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@filePath);
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            //xlApp.Visible = true;
            object missing = Type.Missing;
            foreach (var item in selectedSolution) 
            {
                ((Range)xlWorksheet.Rows[item, Missing.Value]).Interior.ColorIndex = 10;
            }
            xlApp.DisplayAlerts = false;
            
            xlWorkbook.SaveAs(@"c:\users\public\workbook" + extension);

            object misValue = System.Reflection.Missing.Value;
            xlWorkbook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlWorksheet);
            releaseObject(xlWorkbook);
            releaseObject(xlApp);
        }

        private static void releaseObject(object obj)
        {
            try
            {
                
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Unable to release the Object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        internal List<List<int>> withFilter(List<List<int>> sList, String filePath, Decimal[] arrayD, List<int> listI)
        {
            filteredSList.Clear();
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(@filePath);
            
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlApp.Intersect(xlWorksheet.UsedRange, xlWorksheet.Columns["G:G", Type.Missing]);
            Object[,] valueArr = (Object[,])xlRange.get_Value(XlRangeValueDataType.xlRangeValueDefault);
            object missing = Type.Missing;
            allEquals = false;

            foreach (var item in sList)
            {
                y = 0;
                bool final = CheckIfAllEquals(item, y, valueArr, arrayD, listI);
                if (final == true)
                    filteredSList.Add(item);
            }

            xlApp.DisplayAlerts = false;

            object misValue = System.Reflection.Missing.Value;
            xlWorkbook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlWorksheet);
            releaseObject(xlWorkbook);
            releaseObject(xlApp);
            return filteredSList;
        }

        private bool CheckIfAllEquals(List<int> item, int index, object[,] valueArr, Decimal[] arrayD, List<int> listI)
        {
            //Console.WriteLine(item.ElementAt(index));
            //Console.WriteLine(listI.IndexOf(item.ElementAt(index) - 1));
            //Console.WriteLine(arrayD[listI.IndexOf(item.ElementAt(index) - 1)]);
            //Console.WriteLine(valueArr[item[index], 1]);
            String value = Convert.ToString(valueArr[item[index], 1]);
            if (value.Equals("İade alınamayan KDV") || value.Equals("iade alınamayan kdv"))
            {
                allEquals = true;
            }
            else
            {
                allEquals = false;
                return allEquals;
            }
            
            index++;
            if (index < item.Count)
                return allEquals && CheckIfAllEquals(item, index, valueArr, arrayD, listI);
            else
                return allEquals;
        }

    }
}
